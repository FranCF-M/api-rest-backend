//requeriendo express, ejecutando express
const express = require('express');
const app = express();
const morgan = require('morgan');
app.use(morgan('dev')); 
require('./connections');

//middlewares
app.use(express.json());

//settings
app.set('port', process.env.PORT || 8080);

//export routes
app.use(require('./routes/index')); 

//default
app.get('*', (req, res)=>{
   res.send("Hello my Backend")
})

//start server
app.listen(app.get('port'), ()=>{
    console.log(`Server Port http://localhost:${8080}`);
});