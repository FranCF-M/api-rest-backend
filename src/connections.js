const mongoose = require('mongoose');
const string_Connections = 'mongodb://localhost/dispositivos';

mongoose.connect(string_Connections,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
.then((db)=> console.log('Connection Sucessfull'))
.catch((err)=> console.log(err));

module.exports = mongoose;
