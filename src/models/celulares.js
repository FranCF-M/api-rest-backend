const mongoose = require('mongoose');
const {
    Schema
} = mongoose;

const celularesModel = new Schema({
      nombre:{
        type: String,
      },
      marca:{
        type: String,
      },
      ram:{
        type: String,
      }
});

module.exports = mongoose.model('celulares',celularesModel);
